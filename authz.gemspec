# -*- encoding: utf-8 -*-
require File.expand_path('../lib/authz/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Psi"]
  gem.email         = ["psi.xie@gmail.com"]
  gem.description   = %q{Authz}
  gem.summary       = %q{authz}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "authz"
  gem.require_paths = ["lib"]
  gem.version       = Authz::VERSION
end
