require 'authz'
require 'rails'

module Authz
  class Engine < Rails::Engine
      initializer "extend Controller with authz" do |app|
        ActionController::Base.send(:include, Authz::Controller)
        ActionController::Base.helper_method :current_user
        ActionController::Base.helper_method :logged_in?
      end
  end
end