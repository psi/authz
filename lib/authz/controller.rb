module Authz
  module Controller
    def self.included(klass)
      klass.class_eval do
        include InstanceMethods
      end
    end

    module InstanceMethods
      def current_user!
        auth_token =  params[:auth_token] || cookies[:auth_token]
        @current_user = User.find_by_auth_token(auth_token) if auth_token
      end

      def current_user
        @current_user ||= current_user!
        @current_user
      end

      def logout
        if logged_in?
          cookies.delete(:auth_token)
          @current_user = nil
        end
      end

      def logged_in?
        !!current_user
      end

      def request_auth?
         request.fullpath =~ /auth/
      end

      def require_login
        return true if request_auth? #Allow omniauth to work

        if logged_in?
          current_user
        else
          unless request.fullpath == login_path
            session_store_url
            redirect_to login_path
          end
        end
      end

      def session_store_url
        if request.method == "GET"
          session[:return_to_url] = request.url
        end
      end

      def session_store_return_to_url
        session[:return_to_url] = params[:return_to_url] if params[:return_to_url]
      end
    end

  end
end