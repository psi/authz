require "authz/version"

module Authz
  autoload :Controller, 'authz/controller'
  require 'authz/engine' if defined?(Rails) && Rails::VERSION::MAJOR >= 3
end
